#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>
 
RF24 radio(7,8);   // 设置CE, CSN
const byte address[6] = "00001";  //通讯通道地址
void setup()
{
  Serial.begin(9600);
  radio.begin();   //启动nRF模组
  radio.openReadingPipe(0,address);  //设定通道地址
  radio.setPALevel(RF24_PA_MIN);  //设定广播功率
  radio.startListening();  //开始监听无线广播
  }
 
void loop() {
  // put your main code here, to run repeatedly:
  if (radio.available())
  {
    char text = "";
    radio.read(&text,sizeof(text));
    Serial.println(text);
    delay(500);
    }
}

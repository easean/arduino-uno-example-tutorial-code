#define LED_R 6 
#define LED_G 5
#define LED_B 3
    
void setup() {
  pinMode(LED_R, OUTPUT);
  pinMode(LED_G, OUTPUT);
  pinMode(LED_B, OUTPUT);
}

void loop() {
  digitalWrite(LED_R, LOW);
  digitalWrite(LED_G, LOW);
  digitalWrite(LED_B, LOW);
  delay(2000);                // 延迟2秒
  
  digitalWrite(LED_R, HIGH);  //亮白光
  digitalWrite(LED_G, HIGH);
  digitalWrite(LED_B, HIGH);
  delay(2000);                // 延迟2秒

  digitalWrite(LED_R, HIGH);  //亮红光
  digitalWrite(LED_G, LOW);
  digitalWrite(LED_B, LOW);
  delay(2000);                // 延迟2秒

  digitalWrite(LED_R, LOW);  //亮绿光
  digitalWrite(LED_G, HIGH);
  digitalWrite(LED_B, LOW);
  delay(2000);                // 延迟2秒
  
  digitalWrite(LED_R, LOW);  //亮蓝光
  digitalWrite(LED_G, LOW);
  digitalWrite(LED_B, HIGH);
  delay(2000);                // 延迟2秒
}

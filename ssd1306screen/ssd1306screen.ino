#include <Arduino.h>
#include <U8g2lib.h>

#define SCL 12   //A3对应的针脚号
#define SDA 13   //A4对应的针脚号

//下面这行代码就是初始化针脚的，根据自己的接线设置clock和data即可，reset没有连接就用参数U8X8_PIN_NONE
U8G2_SSD1306_128X64_NONAME_1_SW_I2C u8g2(U8G2_R0, /* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display

void drawString(char* str)
{
  u8g2.setFont(u8g2_font_logisoso16_tf);
  u8g2.setCursor(15, 40);
  u8g2.print(str);
}

void draw(char* str)
{
  do {
    drawString(str);
  } while ( u8g2.nextPage() );
}

void setup(void) {
  Serial.begin(9600);
  u8g2.begin();
  u8g2.enableUTF8Print();
}

void loop(void) {
  draw("hello world!");
}

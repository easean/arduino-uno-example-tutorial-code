#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

RF24 radio(7,8);   // 设置 CE,CSN
const byte address[6] = "00001";  //通讯通道地址，可以为任意的5个字符串
void setup()
{
  Serial.begin(9600);
  radio.begin();   //启动nRF模组
  radio.openWritingPipe(address);  //设定通道地址
  radio.setPALevel(RF24_PA_MIN);  //设定广播功率
  radio.stopListening();   //停止侦听，设定成发射模式
  }
 
void loop() {
  // put your main code here, to run repeatedly:
  
  const char text = 'A';
  radio.write(&text, sizeof(text));
  Serial.println(text);
  delay(1000);
}

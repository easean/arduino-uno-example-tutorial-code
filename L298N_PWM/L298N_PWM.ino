#define IN3 6
#define IN4 5

int speed[4] = {100, 150, 200, 250}; //pwm中的占空比，255为100%
int i = 0;

void setup() {
//初始化各IO,模式为OUTPUT 输出模式
pinMode(IN3,OUTPUT);
pinMode(IN4,OUTPUT);
}
 
void loop() {
  for(i = 0; i < 4; i++){ //4挡速度
    //正转
    analogWrite(IN3,speed[i]); 
    analogWrite(IN4,0);  
    delay(1000);   
   
    //stop 停止
    analogWrite(IN3,0);
    analogWrite(IN4,0);  
    delay(2000);  
  }
}

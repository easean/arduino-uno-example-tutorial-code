#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif
 
// 控制 WS2812 灯条的引脚编号
#define PIN 13
 
//定义控制的 LED 数量
#define NUMPIXELS 4
 
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
                  //[R,G,B]值，对应16个灯珠各自的颜色
int image[16][3] = {{1,0,0},
                    {5,2,0},
                    {10,15,0},
                    {0,5,0},
                    {0,3,5},
                    {0,0,5},
                    {2,0,5},
                    {2,2,2},
                    {5,0,0},
                    {3,2,0},
                    {2,3,0},
                    {0,5,0},
                    {0,2,5},
                    {0,0,5},
                    {3,0,5},
                    {5,5,5}
  };
 
//点亮相邻 LED 之间的延迟，单位毫秒
#define DELAYVAL 50
 
void setup() {
  // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
  // Any other board, you can remove this part (but no harm leaving it):
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.
 
  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
}
 
void loop() {
  pixels.clear(); // Set all pixel colors to 'off'
 
  // The first NeoPixel in a strand is #0, second is 1, all the way up
  // to the count of pixels minus one.
  for(int i=0; i<NUMPIXELS; i++) { // For each pixel...
 
    // pixels.Color() takes RGB values, from 0,0,0 up to 255,255,255
    // Here we're using a moderately bright green color:
    //pixels.setPixelColor(i, pixels.Color(i*30, 0, i*10 + 30));
    pixels.setPixelColor(i, pixels.Color(image[i][0], image[i][1], image[i][2]));
 
    pixels.show();   // Send the updated pixel colors to the hardware.
 
    delay(DELAYVAL); // Pause before next pass through loop
  }
  pixels.clear();
  delay(2000);
}

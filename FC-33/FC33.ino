#define LEDPIN A0  //接LED的针脚
#define SWITCH 11  //接开关的针脚

void setup() {
  // put your setup code here, to run once:
  pinMode(LEDPIN, OUTPUT);
  pinMode(SWITCH, INPUT);
}

int input = 0;
void loop() {
  // put your main code here, to run repeatedly:
  input = digitalRead(SWITCH);
  if(input == HIGH){
    digitalWrite(LEDPIN, HIGH);
  }
  else{
    digitalWrite(LEDPIN, LOW);  
  }
}

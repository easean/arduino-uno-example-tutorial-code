#define LEDPIN 4
#define SWITCHPIN A0
#define THRESHOLD 200   //检测有没有障碍物的临界值

int iv; //可以理解成输入电压，arduino模拟针脚能获取到0~5V电压，将这个范围分成1024分，每份0.48mv。
        //这里获取到的iv只是算的电压在这1024份里的占比，实际电压要乘0.48.
        //作为程序判断逻辑不需要计算真实电压，只需要知道有没有感应到附近有反射光的物体就可以。所以没有乘0.48
void setup(){
  pinMode(LEDPIN,OUTPUT);
  Serial.begin(9600);
}

void loop(){
  iv = analogRead(SWITCHPIN);
  if(iv > THRESHOLD){     //
    digitalWrite(LEDPIN, HIGH);
  }
  else{
    digitalWrite(LEDPIN, LOW);
  }
  Serial.println(iv);
  //delay(1000);
}

#include<Servo.h>

#define SERVO_PIN 13     //舵机数据线使用的引脚

Servo myservo;

int pos = 0;

void setup() {
  // put your setup code here, to run once:
  myservo.attach(SERVO_PIN);
}

void loop() {
  //控制电机正转
  for(pos = 0; pos < 180; pos += 1){
    myservo.write(pos);
    delay(5);  
  }
  
  //控制电机反转
  for(pos = 180; pos >=1; pos -= 1){
    myservo.write(pos);
    delay(5);  
  }
}

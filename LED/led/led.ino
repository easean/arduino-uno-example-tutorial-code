#define BTNPIN 13  //button pin
#define LEDPIN 8    //LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
  Serial.begin(9600);
  // initialize the pushbutton pin as an input:
  pinMode(BTNPIN, INPUT);
  pinMode(LEDPIN, OUTPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(BTNPIN);
  if (buttonState == HIGH) {
    digitalWrite(LEDPIN, HIGH);  //按键按下时led灯亮
  }
  else if (buttonState == LOW) {
    digitalWrite(LEDPIN, LOW);   //按键松开时led灯灭
  }
}

#define BTNPIN 12    //读取按键状态的针脚

int buttonState = 0;         // variable for reading the pushbutton status
char buf[32] = {0};          //申请了一个32Byte的内存空间，用来存放打印提示字符串。

void setup() {
  Serial.begin(9600);
  // initialize the pushbutton pin as an input:
  pinMode(BTNPIN, INPUT);
  sprintf(buf, "button pressed at PIN: [%d]", BTNPIN);  //将提示语句按照格式写入buf中。
  //提示字符串的长度如果超过32Byte就修改buf的容量。够用就好，不要申请太多。
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(BTNPIN);
  if (buttonState == HIGH) {
    Serial.println(buf);    //将buf中存放的内容通过串口打印输出。
  }
}

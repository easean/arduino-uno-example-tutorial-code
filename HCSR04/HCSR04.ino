#define Trig 13 //引脚Trig 连接 13
#define Echo 12 //引脚Echo 连接 12

float dis; //计算最后的距离
float timer; //计时器

void setup() {
  Serial.begin(9600);
  pinMode(Trig, OUTPUT);
  pinMode(Echo, INPUT);
}

void loop() {
  //控制发射超声波
  digitalWrite(Trig, LOW); 
  delayMicroseconds(2);   
  digitalWrite(Trig,HIGH); 
  delayMicroseconds(10);  
  digitalWrite(Trig, LOW); 

  //发送完声波开始计时，同时等待声波返回。
  timer = float(pulseIn(Echo, HIGH)); 
  //Echo引脚电平为HIGH时，pulseIn函数开始计算时间
  //当检测到返回的声波时，Echo引脚电平为LOW，pulseIn函数停止计时。
  //声速是:340m/s = 0.034cm/μs
  //距离：0.034*temp/2 = 0.017 * temp cm
  
  dis = timer * 0.017f; //把回波时间换算成cm

  //打印输出测距结果
  Serial.print("time:");
  Serial.print(timer);
  Serial.print("μs");
  Serial.print(" <--> distance:");
  Serial.print(dis);
  Serial.println("cm");
  
  delay(1000);
}

#define IN3 7
#define IN4 6 
 
void setup() {
//初始化各IO,模式为OUTPUT 输出模式
pinMode(IN3,OUTPUT);
pinMode(IN4,OUTPUT);
}
 
void loop() {
  //正转
  digitalWrite(IN3,HIGH); 
  digitalWrite(IN4,LOW);  
  delay(1000);   
 
 //stop 停止
 digitalWrite(IN3,LOW);
 digitalWrite(IN4,LOW);  
 delay(2000);  
   
  //反转
  digitalWrite(IN3,LOW);
  digitalWrite(IN4,HIGH);  
  delay(1000);  

   //stop 停止
  digitalWrite(IN3,LOW);
  digitalWrite(IN4,LOW);  
  delay(2000);
}

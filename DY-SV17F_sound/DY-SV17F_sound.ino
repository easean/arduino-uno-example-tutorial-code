const int buttonPin = 13;     // the number of the pushbutton pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status
int ispressed = 0;
unsigned char setFlash[5]= {0xAA, 0x0B, 0x01, 0x02, 0xB8}; //DY-SV17F的串口控制协议里的一个控制命令，功能是设置从板载flash上播放音频
unsigned char hexdata[4]= {0xAA, 0x02,0x00,0xAC}; //DY-SV17F的串口控制协议里的一个控制命令，功能是播放flash的音频。
//DY-SV17F的串口控制协议请查看DY-SV17F的技术文档。

void setup() {
  Serial.begin(9600);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);

  //Serial.write(setFlash, 5);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);
  
  //按键按下后又抬起才出发播放
  if (buttonState == HIGH) { 
    ispressed = 1;    
  }else
  {
    if(ispressed == 1)
      {
        Serial.println("button pressed");
        //串口控制语音播放代码：
        Serial.write(setFlash, 5);
        delay(3);
        Serial.write(hexdata, 4);
      }
    ispressed = 0;
  }
}

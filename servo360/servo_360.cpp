#include "servo_360.h"
#include <Arduino.h>

Servo_360::Servo_360()
{
  
}
  
Servo_360::~Servo_360()
{
  
}

void Servo_360::initialize(int pin, int max_step, int step_delay)
{
  this->servo_pin = pin;
  this->max_step = max_step;
  this->step_delay = step_delay;
  this->step_count = 0;
  //this->servo_speed = servo_speed;
  servo360.attach(this->servo_pin);  
}

int Servo_360::forward()
{
  if(this->step_count < this->max_step)
  {
    servo360.write(0);
    delay(this->step_delay);
    servo360.write(90);
    this->step_count++;
    //Serial.println(this->step_count);
    delay(500); //舵机转动一下停止的时间
  }
  
  return this->step_count;
}

int Servo_360::backward()
{
  if(this->step_count > 0)
  {
    servo360.write(180);
    delay(this->step_delay);
    servo360.write(90);
    this->step_count--;
    //Serial.println(this->step_count);
    delay(500); //舵机转动一下停止的时间
  }
  
  return this->step_count;
}

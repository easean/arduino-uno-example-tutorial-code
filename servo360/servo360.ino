#include<Servo.h>
#include"servo_360.h"

#define SERVO180_PIN 7     //舵机数据线使用的引脚
#define SERVO360_PIN 8     //舵机数据线使用的引脚

#define CYCLE_DELAY 30 //360舵机走一步所需时间
#define CYCLE_STEP 20 //360舵机要走的步数，根据测试得出走一圈大概需要20步

Servo_360 servo360;
int dir = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);  
  servo360.initialize(SERVO360_PIN, CYCLE_STEP, CYCLE_DELAY);
}

int s = 0;
void loop() {
  //控制电机正转
  if(dir == 0){
    s = servo360.forward();
    if(s == 20)
    {
      dir = 1;
    }
  }  
  else if(dir == 1)
  {
    s = servo360.backward();
    if(s == 0)
    {
      dir = 0;
    }
  }
}

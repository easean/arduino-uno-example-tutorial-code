/*
 * 360°舵机的特点：
 * 
 * 可以无限旋转，旋转方向由servo.write(int speed)函数中的speed来设定。
 * 
 * speed小于90时正转， speed == 90舵机停止转动， speed大于90反转。
 * 
 * speed越接近90速度越慢。实际测试中speed在90±5°左右的时候就是停止状态。  
 * 
 * 360°舵机只要servo.write(int speed)之后就会一直以速度speed转动。想停下
 * 来需要调用servo.write(90)；
 * 
 * 360°舵机的操作库，因为360°舵机能一直无限绕圈，舵机因为接有线材，当舵机装在
 * 云台上， * 如果任由舵机 * 无限绕圈的话，线会绕死在云台上，所以做了这个库来
 * 限制舵机只能转360°（左右），舵机转动一圈需要的 * 步数和步长是测试得到的，
 * 可以更改。转速本质上也可以设置，修改转速之后应该步数和步长需要重新调整。
 * 
*/

#ifndef SERVO_360_H
#define SERVO_360_H

#include<Servo.h>

class Servo_360
{


  public:
  void initialize(int pin, int max_step, int step_delay);
  int forward();  //返回当前步数计数，达到最大步数就不继续向前转动
  int backward(); //返回当前步数计数，回到初始位置就不继续向后转动
  Servo_360();
  ~Servo_360();
  
  private:
  int servo_pin;   //电机控制针脚
  //int servo_speed; //电机转速，最快为0，90为停，大于90为反转。速度越接近90转速越慢。
  int step_count;  //步数计数器
  int max_step;    //电机能够行走的最大步数
  int step_delay;  //每走一步所需的时间

  Servo servo360;
};

#endif

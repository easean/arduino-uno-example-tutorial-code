#define PIN_R 6 
#define PIN_G 5
#define PIN_B 3
#define INTERVAL 10 //亮度持续时间

void setup(){ 
  pinMode(PIN_R,OUTPUT); //设置引脚模式
  pinMode(PIN_G,OUTPUT);
  pinMode(PIN_B,OUTPUT);
  analogWrite(PIN_R,0); 
  analogWrite(PIN_G,0);
  analogWrite(PIN_B,0);
}

void loop() {
  //红色逐渐变亮 
  for(int val=0; val<255; val++){ 
    analogWrite(PIN_R,val); //给指定引脚写入数据
    analogWrite(PIN_G,0);
    analogWrite(PIN_B,0);
    delay(INTERVAL); 
  }
  //红色逐渐变暗 
  for(int val=254; val>=0; val--){ 
    analogWrite(PIN_R,val);
    analogWrite(PIN_G,0); 
    analogWrite(PIN_B,0);  
    delay(INTERVAL); 
  }
  //绿色逐渐变亮 
  for(int val=0; val<255; val++){ 
    analogWrite(PIN_R,0); //给指定引脚写入数据
    analogWrite(PIN_G,val);
    analogWrite(PIN_B,0);
    delay(INTERVAL); 
  }
  //绿色逐渐变暗 
  for(int val=254; val>=0; val--){ 
    analogWrite(PIN_R,0);
    analogWrite(PIN_G,val); 
    analogWrite(PIN_B,0);  
    delay(INTERVAL); 
  }
  //蓝色逐渐变亮 
  for(int val=0; val<255; val++){ 
    analogWrite(PIN_R,0); //给指定引脚写入数据
    analogWrite(PIN_G,0);
    analogWrite(PIN_B,val);
    delay(INTERVAL); 
  }
  //蓝色逐渐变暗 
  for(int val=254; val>=0; val--){ 
    analogWrite(PIN_R,0);
    analogWrite(PIN_G,0); 
    analogWrite(PIN_B,val);  
    delay(INTERVAL); 
  }
}
